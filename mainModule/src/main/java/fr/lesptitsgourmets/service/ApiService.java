package fr.lesptitsgourmets.service;

import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.util.Log;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.lesptitsgourmets.model.Recipe;
import fr.lesptitsgourmets.model.RecipeDetail;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class ApiService {
    private static String API_URL = "https://les-ptits-gourmets.herokuapp.com";

    public static List<Recipe> getAllRecipes() {
        List<Recipe> allRecipes = new ArrayList<>();

        AsyncTask<String, Void, String> response = new RequestService().execute(API_URL + "/recipes");
        if (response != null) {
            try {
                if (response.get() == null) {
                    return null;
                }
                JSONObject jsonResponse = new JSONObject(response.get());
                JSONArray data = jsonResponse.getJSONArray("data");
                ObjectMapper mapper = new ObjectMapper();
                for (int i = 0; i < data.length(); i++) {
                    JSONObject jsonRecipe = data.getJSONObject(i);
                    Recipe recipe = mapper.readValue(jsonRecipe.toString(), Recipe.class);
                    allRecipes.add(recipe);
                }

                return allRecipes;
            } catch (JSONException | IOException | InterruptedException | ExecutionException e) {
                Log.e("ERROR", e.getMessage(), e);
                return null;
            }
        } else {
            return null;
        }
    }

    public static RecipeDetail getRecipeById(String id) {
        AsyncTask<String, Void, String> response = new RequestService().execute(API_URL + "/recipe/" + id);
        if (response != null) {
            try {
                JSONObject jsonResponse = new JSONObject(response.get());
                JSONObject data = jsonResponse.getJSONArray("data").getJSONObject(0);
                ObjectMapper mapper = new ObjectMapper();
                RecipeDetail recipe = mapper.readValue(data.toString(), RecipeDetail.class);

                return recipe;
            } catch (JSONException | IOException | InterruptedException | ExecutionException e) {
                Log.e("ERROR", e.getMessage(), e);
                return null;
            }
        } else {
            return null;
        }
    }

    public static Drawable LoadImageFromWebOperations(String url) {
        try {
            InputStream is = (InputStream) new URL(url).getContent();
            Drawable d = Drawable.createFromStream(is, "src name");
            return d;
        } catch (Exception e) {
            System.out.println("Exc=" + e);
            return null;
        }

    }
}
