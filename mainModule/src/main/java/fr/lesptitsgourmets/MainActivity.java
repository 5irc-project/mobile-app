package fr.lesptitsgourmets;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import fr.lesptitsgourmets.databinding.ActivityMainBinding;
import fr.lesptitsgourmets.fragment.ListRecipeFragment;
import fr.lesptitsgourmets.fragment.RecipeDetailFragment;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding binding;
    private RecipeDetailFragment detailFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        showStartup();
    }

    public void showDetails(View view) {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        Bundle bundle = new Bundle();
        bundle.putString("recipe_id", view.getTag().toString());
        detailFragment = new RecipeDetailFragment();
        detailFragment.setArguments(bundle);
        transaction.replace(R.id.fragment_container, detailFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    public void showStartup() {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        ListRecipeFragment fragment = new ListRecipeFragment();
        transaction.replace(R.id.fragment_container, fragment);
        transaction.commit();
    }

    public void readRecipe(View view) {
        detailFragment.readRecipe(view);
    }

    public void readStep(View view) {
        detailFragment.readStep(view);
    }
}
