package fr.lesptitsgourmets.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import fr.lesptitsgourmets.R;
import fr.lesptitsgourmets.adapter.RecipeAdapter;
import fr.lesptitsgourmets.databinding.ListRecipeBinding;
import fr.lesptitsgourmets.model.Recipe;
import fr.lesptitsgourmets.service.ApiService;

public class ListRecipeFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             Bundle savedInstanceState) {
        ListRecipeBinding binding =
                DataBindingUtil.inflate(inflater,R.layout.list_recipe,container,false);

        binding.recipeList.setLayoutManager(new
                LinearLayoutManager(binding.getRoot().getContext()));

        List<Recipe> recipes = ApiService.getAllRecipes();

        binding.recipeList.setAdapter(new RecipeAdapter(recipes));

        return binding.getRoot();
    }

}
