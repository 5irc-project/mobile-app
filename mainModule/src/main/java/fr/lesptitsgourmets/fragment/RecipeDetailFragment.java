package fr.lesptitsgourmets.fragment;

import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import fr.lesptitsgourmets.R;
import fr.lesptitsgourmets.adapter.IngredientAdapter;
import fr.lesptitsgourmets.adapter.StepAdapter;
import fr.lesptitsgourmets.databinding.RecipeDetailBinding;
import fr.lesptitsgourmets.model.RecipeDetail;
import fr.lesptitsgourmets.model.RecipeStep;
import fr.lesptitsgourmets.service.ApiService;
import fr.lesptitsgourmets.viewModel.RecipeDetailViewModel;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Locale;

public class RecipeDetailFragment extends Fragment implements
        TextToSpeech.OnInitListener {

    private RecipeDetail recipe_detail;
    private TextToSpeech tts;
    private FloatingActionButton floatingButton;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             Bundle savedInstanceState) {
        final RecipeDetailBinding binding =
                DataBindingUtil.inflate(inflater, R.layout.recipe_detail, container, false);

        tts = new TextToSpeech(this.getContext(), this);

        Bundle bundle = this.getArguments();
        String id = bundle.get("recipe_id").toString();
        readDetails(id);
        floatingButton = binding.getRoot().findViewById(R.id.floating_button_detail);
        floatingButton.setImageResource(R.drawable.read);

        new DownloadImageTask((ImageView) binding.getRoot().findViewById(R.id.recipe_photo))
                .execute(recipe_detail.getPhotoPath());


        binding.setJvm(new RecipeDetailViewModel(recipe_detail));

        binding.recipeIngredients.setLayoutManager(new
                GridLayoutManager(binding.getRoot().getContext(), 2, GridLayoutManager.VERTICAL, false));
        binding.recipeIngredients.setAdapter(new IngredientAdapter(recipe_detail.getIngredientrecipe()));

        binding.recipeSteps.setLayoutManager(new
                LinearLayoutManager(binding.getRoot().getContext()));
        binding.recipeSteps.setAdapter(new StepAdapter(recipe_detail.getSteps()));

        return binding.getRoot();
    }

    @Override
    public void onDestroy() {
        if (tts != null) {
            tts.stop();
            tts.shutdown();
        }
        super.onDestroy();
    }

    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {
            int result = tts.setLanguage(Locale.FRANCE);

            if (result == TextToSpeech.LANG_MISSING_DATA
                    || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("TTS", "This Language is not supported");
            }
        } else {
            Log.e("TTS", "Initilization Failed!");
        }
    }

    private void readDetails(String id) {
        RecipeDetail detail = ApiService.getRecipeById(id);
        this.recipe_detail = detail;
    }

    public void readRecipe(View view) {
        if (tts.isSpeaking()) {
            floatingButton.setImageResource(R.drawable.read);
            tts.stop();
        } else {
            floatingButton.setImageResource(R.drawable.stop);
            tts.speak("étapes à suivres :", TextToSpeech.QUEUE_ADD, null, "step");
            tts.playSilentUtterance(1000, TextToSpeech.QUEUE_ADD, "step");
            for (RecipeStep step : recipe_detail.getSteps()) {
                tts.speak(step.toString(), TextToSpeech.QUEUE_ADD, null, "step");
                tts.playSilentUtterance(500, TextToSpeech.QUEUE_ADD, "step");
            }
        }
    }

    public void readStep(View view) {
        floatingButton.setImageResource(R.drawable.stop);
        String noOrder = view.getTag().toString();
        for (RecipeStep step : recipe_detail.getSteps()) {
            if (String.valueOf(step.getNoorder()).equals(noOrder)) {
                tts.speak(step.toString(), TextToSpeech.QUEUE_FLUSH, null, "step");
            }
        }
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }
}
