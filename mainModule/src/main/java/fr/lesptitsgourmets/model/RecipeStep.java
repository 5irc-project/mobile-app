package fr.lesptitsgourmets.model;

public class RecipeStep {
    private Integer id;
    private int noorder;
    private int duration;
    private String description;
    private Type type;

    public RecipeStep() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getNoorder() {
        return noorder;
    }

    public void setNoorder(int noorder) {
        this.noorder = noorder;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "étape " + noorder + " : " + description;
    }
}
