package fr.lesptitsgourmets.model;

public class Recipe {
    private long id;
    private String name;
    private String description;
    private String photoPath;
    private int score;
    private User creator;

    public Recipe() {
    }

    public Recipe(String nom, String description, String photoPath, int score) {
        this.name = nom;
        this.description = description;
        this.photoPath = photoPath;
        this.score = score;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getPhotoPath() {
        return photoPath;
    }

    public int getScore() {
        return score;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }
}
