package fr.lesptitsgourmets.model;

import java.util.ArrayList;
import java.util.List;

public class RecipeDetail {
    private int id;
    private String name;
    private String description;
    private int score;
    private String photoPath;
    private User creator;
    private List<RecipeStep> steps = new ArrayList<>();
    private List<IngredientRecipe> ingredientrecipe = new ArrayList<>();
    private List<TagRecipe> tagRecipes = new ArrayList<>();

    public RecipeDetail() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getPhotoPath() {
        return photoPath;
    }

    public void setPhotoPath(String photoPath) {
        this.photoPath = photoPath;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public List<RecipeStep> getSteps() {
        return steps;
    }

    public void setSteps(List<RecipeStep> steps) {
        this.steps = steps;
    }

    public List<IngredientRecipe> getIngredientrecipe() {
        return ingredientrecipe;
    }

    public void setIngredientrecipe(List<IngredientRecipe> ingredientrecipe) {
        this.ingredientrecipe = ingredientrecipe;
    }

    public List<TagRecipe> getTagRecipes() {
        return tagRecipes;
    }

    public void setTagRecipes(List<TagRecipe> tagRecipes) {
        this.tagRecipes = tagRecipes;
    }

    @Override
    public String toString() {
        return "RecipeDetail{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", score=" + score +
                ", photoPath='" + photoPath + '\'' +
                ", creator=" + creator +
                ", steps=" + steps +
                ", ingredientrecipe=" + ingredientrecipe +
                ", tagRecipes=" + tagRecipes +
                '}';
    }
}
