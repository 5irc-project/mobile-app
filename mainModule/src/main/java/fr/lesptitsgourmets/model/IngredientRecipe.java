package fr.lesptitsgourmets.model;

public class IngredientRecipe {

    private Ingredient ingredient;
    private double quantity;

    public IngredientRecipe() {
    }

    public Ingredient getIngredient() {
        return ingredient;
    }

    public void setIngredient(Ingredient ingredient) {
        this.ingredient = ingredient;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }
}
