package fr.lesptitsgourmets.model;

public enum Type {
    preparation,
    cooking,
    waiting
}
