package fr.lesptitsgourmets.model;

public class TagRecipe {
    private Tag tag;

    public TagRecipe() {
    }

    public Tag getTag() {
        return tag;
    }

    public void setTag(Tag tag) {
        this.tag = tag;
    }
}
