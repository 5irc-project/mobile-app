package fr.lesptitsgourmets.json;

public class Recipe_detail {
    public static String recipe = "{\n" +
            "  \"id\": 3,\n" +
            "  \"name\": \"Tiramisu (Recette originale)\",\n" +
            "  \"description\": \"Il existe de nombreuses recettes de tiramisu. Celle-ci est la recette originale (ou en tous les cas, l'une des recettes pouvant prétendre l'être!). NB: il y a bien de l'alcool dans le tiramisu mais il s'agit de Marsala sec (ni aux oeufs, ni à l'amande) mélangé au café très fort.\",\n" +
            "  \"score\": \"9\",\n" +
            "  \"photo_path\": \"tiramisu.jpg\",\n" +
            "  \"user_account_id\": 1,\n" +
            "  \"steps\": [\n" +
            "    {\n" +
            "      \"noorder\": 1,\n" +
            "      \"description\": \"Séparer les blancs des jaunes d'oeufs.\",\n" +
            "      \"type\": \"preparation\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"noorder\": 2,\n" +
            "      \"description\": \"Mélanger les jaunes avec le sucre roux et le sucre vanillé.\",\n" +
            "      \"type\": \"preparation\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"noorder\": 3,\n" +
            "      \"description\": \"Ajouter le mascarpone au fouet.\",\n" +
            "      \"type\": \"preparation\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"noorder\": 4,\n" +
            "      \"description\": \"Monter les blancs en neige et les incorporer délicatement à la spatule au mélange précédent. Réserver.\",\n" +
            "      \"duration\": 5,\n" +
            "      \"type\": \"preparation\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"noorder\": 5,\n" +
            "      \"description\": \"Mouiller les biscuits dans le café rapidement avant d'en tapisser le fond du plat.\",\n" +
            "      \"type\": \"preparation\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"noorder\": 6,\n" +
            "      \"description\": \"Recouvrir d'une couche de crème au mascarpone puis répéter l'opération en alternant couche de biscuits et couche de crème en terminant par cette dernière.\",\n" +
            "      \"duration\": 10,\n" +
            "      \"type\": \"preparation\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"noorder\": 7,\n" +
            "      \"description\": \"Saupoudrer de cacao.\",\n" +
            "      \"type\": \"preparation\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"noorder\": 8,\n" +
            "      \"duration\": 240,\n" +
            "      \"description\": \"Mettre au réfrigérateur 4 heures minimum puis déguster frais.\",\n" +
            "      \"type\": \"rest\"\n" +
            "    }\n" +
            "  ],\n" +
            "  \"ingredients\": [\n" +
            "    {\n" +
            "      \"name\": \"Oeufs\",\n" +
            "      \"unit\": \" \",\n" +
            "      \"quantity\": 3\n" +
            "    },\n" +
            "    {\n" +
            "      \"name\": \"Sucre roux\",\n" +
            "      \"unit\": \"g\",\n" +
            "      \"quantity\": 100\n" +
            "    },\n" +
            "    {\n" +
            "      \"name\": \"Sucre vanillé\",\n" +
            "      \"unit\": \"sachet\",\n" +
            "      \"quantity\": 1\n" +
            "    },\n" +
            "    {\n" +
            "      \"name\": \"Mascarpone\",\n" +
            "      \"unit\": \"g\",\n" +
            "      \"quantity\": 250\n" +
            "    },\n" +
            "    {\n" +
            "      \"name\": \"Biscuits à la cuillère\",\n" +
            "      \"unit\": \" \",\n" +
            "      \"quantity\": 24\n" +
            "    },\n" +
            "    {\n" +
            "      \"name\": \"Café noir non sucré\",\n" +
            "      \"unit\": \"cl\",\n" +
            "      \"quantity\": 50\n" +
            "    },\n" +
            "    {\n" +
            "      \"name\": \"Cacao amer\",\n" +
            "      \"unit\": \"g\",\n" +
            "      \"quantity\": 30\n" +
            "    }\n" +
            "  ],\n" +
            "  \"tags\": [\n" +
            "    {\n" +
            "      \"name\": \"Dessert\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"name\": \"Sucre\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"name\": \"Rital du cul\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"name\": \"Gourmand\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"name\": \"Serbe sexuel\"\n" +
            "    }\n" +
            "  ]\n" +
            "}";
}
