package fr.lesptitsgourmets.viewModel;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import fr.lesptitsgourmets.model.IngredientRecipe;

public class IngredientViewModel  extends BaseObservable {
    private IngredientRecipe ingredient;

    public IngredientViewModel(IngredientRecipe ingredient) {
        this.ingredient = ingredient;
    }

    @Bindable
    public String getDisplay() {
        String res = "";
        res += "• ";
        res += ingredient.getIngredient().getName();
        res += " : ";
        res += String.valueOf(ingredient.getQuantity());
        res += " " ;
        res += ingredient.getIngredient().getUnit().getName();
        return res;
    }
}
