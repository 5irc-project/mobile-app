package fr.lesptitsgourmets.viewModel;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import fr.lesptitsgourmets.model.Recipe;

public class RecipeViewModel extends BaseObservable {
    private Recipe recipe;

    public RecipeViewModel(Recipe recipe) {
        this.recipe = recipe;
    }

    @Bindable
    public String getId() { return String.valueOf(recipe.getId()); }

    @Bindable
    public String getName() {
        return recipe.getName();
    }

    @Bindable
    public String getDescription() {
        return recipe.getDescription();
    }

    @Bindable
    public String getPhotoPath() {
        return recipe.getPhotoPath();
    }

    @Bindable
    public String getScore() {
        return String.valueOf(recipe.getScore());
    }
}
