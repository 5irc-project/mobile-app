package fr.lesptitsgourmets.viewModel;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import fr.lesptitsgourmets.model.RecipeDetail;

public class RecipeDetailViewModel extends BaseObservable {
    private RecipeDetail details;

    public RecipeDetailViewModel(RecipeDetail details) {
        this.details = details;
    }

    @Bindable
    public String getScore() {
        return String.valueOf(details.getScore());
    }

    @Bindable
    public String getName() {
        return details.getName();
    }

    @Bindable
    public String getDescription() {
        return details.getDescription();
    }
}
