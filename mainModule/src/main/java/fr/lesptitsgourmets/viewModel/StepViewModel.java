package fr.lesptitsgourmets.viewModel;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import fr.lesptitsgourmets.model.RecipeStep;

public class StepViewModel extends BaseObservable {
    private RecipeStep step;

    public StepViewModel(RecipeStep step) {
        this.step = step;
    }

    @Bindable
    public String getId() {
        return String.valueOf(step.getNoorder());
    }

    @Bindable
    public String getDescription() {
        String res = "";
        res += step.getNoorder();
        res += ") ";
        res += step.getDescription();
        return res;
    }

}
