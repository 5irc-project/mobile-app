package fr.lesptitsgourmets.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import fr.lesptitsgourmets.R;
import fr.lesptitsgourmets.databinding.IngredientViewBinding;
import fr.lesptitsgourmets.model.IngredientRecipe;
import fr.lesptitsgourmets.viewModel.IngredientViewModel;

public class IngredientAdapter extends RecyclerView.Adapter<IngredientAdapter.BindingHolder> {
    private List<IngredientRecipe> ingredientRecipes;

    public IngredientAdapter(List<IngredientRecipe> ingredientRecipes) {
        this.ingredientRecipes = ingredientRecipes;
    }

    @Override
    public IngredientAdapter.BindingHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        IngredientViewBinding binding =
                DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                        R.layout.ingredient_view, parent, false);

        return new IngredientAdapter.BindingHolder(binding);
    }

    @Override
    public void onBindViewHolder(IngredientAdapter.BindingHolder holder, int
            position) {
        IngredientViewBinding binding = holder.binding;

        binding.setJvm(new IngredientViewModel(ingredientRecipes.get(position)));
    }

    @Override
    public int getItemCount() {
        return (ingredientRecipes == null ? 0 : ingredientRecipes.size());
    }

    static class BindingHolder extends RecyclerView.ViewHolder {
        private IngredientViewBinding binding;

        BindingHolder(IngredientViewBinding binding) {
            super(binding.ingredientItem);
            this.binding = binding;
        }
    }
}
