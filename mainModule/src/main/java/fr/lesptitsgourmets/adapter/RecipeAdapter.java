package fr.lesptitsgourmets.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import fr.lesptitsgourmets.R;
import fr.lesptitsgourmets.databinding.RecipeViewBinding;
import fr.lesptitsgourmets.model.Recipe;
import fr.lesptitsgourmets.viewModel.RecipeViewModel;

public class RecipeAdapter extends RecyclerView.Adapter<RecipeAdapter.BindingHolder> {
    private List<Recipe> recipes = new ArrayList<>();

    public RecipeAdapter(List<Recipe> recipes) {
        if (recipes != null) {
            this.recipes = recipes;
        }
    }

    @Override
    public BindingHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecipeViewBinding binding =
                DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                        R.layout.recipe_view, parent, false);

        return new BindingHolder(binding);
    }

    @Override
    public void onBindViewHolder(RecipeAdapter.BindingHolder holder, int
            position) {
        RecipeViewBinding binding = holder.binding;

        binding.setJvm(new RecipeViewModel(recipes.get(position)));
    }

    @Override
    public int getItemCount() {
        return recipes.size();
    }

    static class BindingHolder extends RecyclerView.ViewHolder {
        private RecipeViewBinding binding;

        BindingHolder(RecipeViewBinding binding) {
            super(binding.recipeItem);
            this.binding = binding;
        }
    }


}
