package fr.lesptitsgourmets.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import fr.lesptitsgourmets.R;
import fr.lesptitsgourmets.databinding.StepViewBinding;
import fr.lesptitsgourmets.model.RecipeStep;
import fr.lesptitsgourmets.viewModel.StepViewModel;

public class StepAdapter extends RecyclerView.Adapter<StepAdapter.BindingHolder> {
    private List<RecipeStep> steps;

    public StepAdapter(List<RecipeStep> steps) {
        this.steps = steps;
    }

    @Override
    public StepAdapter.BindingHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        StepViewBinding binding =
                DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                        R.layout.step_view, parent, false);

        return new StepAdapter.BindingHolder(binding);
    }

    @Override
    public void onBindViewHolder(StepAdapter.BindingHolder holder, int
            position) {
        StepViewBinding binding = holder.binding;

        binding.setJvm(new StepViewModel(steps.get(position)));
    }

    @Override
    public int getItemCount() {
        return (steps == null ? 0 : steps.size());
    }

    static class BindingHolder extends RecyclerView.ViewHolder {
        private StepViewBinding binding;

        BindingHolder(StepViewBinding binding) {
            super(binding.stepItem);
            this.binding = binding;
        }
    }
}
